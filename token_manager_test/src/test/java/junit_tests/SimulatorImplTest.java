package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import token_manager_test.SimulatorImpl;

public class SimulatorImplTest {

	private SimulatorImpl sim;
	
	@Before
	public void setUp() {
		this.sim = new SimulatorImpl();
	}
	
	@Test
	public void postRequestTokenUnregisteredCustomerTest() {
		Response response = sim.postRequestToken();
		
		String message = "";
		try {
			JSONObject json = new JSONObject(response.readEntity(String.class));
			message = (String) json.get("message");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		assertEquals(403, response.getStatus());
		assertEquals("CPR number not registered as a customer", message);
	}
	

}