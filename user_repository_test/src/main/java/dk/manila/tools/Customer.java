package dk.manila.tools;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Class representing a customer of DTUPay
 * 
 * @author Alexandre Délèze
 *
 */
public final class Customer implements Serializable {
	private static final long serialVersionUID = -3417873156899953847L;
	private final CprNumber cpr;
	private final String first_name;
	private final String last_name;
	private final String bank_account_id;
	private final Set<TokenId> tokens;

	/**
	 * Constructor of the class Customer
	 * 
	 * @param cpr
	 *            the CprNumber of the customer
	 * @param firstname
	 *            the first name of the customer
	 * @param lastname
	 *            the last name of the customer
	 * @param bank_account_id
	 *            the identifier of the bank account
	 * @param tokens
	 *            the set containing all the tokens
	 */
	public Customer(CprNumber cpr, String firstname, String lastname, String bank_account_id, Set<TokenId> tokens) {
		this.cpr = cpr;
		this.first_name = firstname;
		this.last_name = lastname;
		this.bank_account_id = bank_account_id;
		this.tokens = new HashSet<>(tokens);
	}

	/**
	 * Getter of the first name of the customer
	 * 
	 * @return first name of the customer
	 */
	public String getFirstName() {
		return first_name;
	}

	/**
	 * Getter of the last name of the customer
	 * 
	 * @return last name of the customer
	 */
	public String getLastName() {
		return last_name;
	}

	/**
	 * Getter of the CPR of the customer
	 * 
	 * @return CPR of the customer
	 */
	public CprNumber getCpr() {
		return cpr;
	}

	/**
	 * Getter of the identifier of the bank account of the customer
	 * 
	 * @return identifier of the bank account of the customer
	 */
	public String getBankAccountId() {
		return bank_account_id;
	}

	/**
	 * Getter of the set containing all the tokens of the customer
	 * 
	 * @return set containing all the tokens of the customer
	 */
	public Set<TokenId> getTokenSet() {
		return new HashSet<>(tokens);
	}

	/**
	 * Add the element tokenId to the collection of tokens of the customer
	 * 
	 * @param tokenId
	 *            token we want to add to the collection of tokens of the customer
	 */
	public void addToken(TokenId tokenId) {
		tokens.add(tokenId);

	}

	/**
	 * Add all the elements of the tokenIdSet to the collection of tokens of the
	 * customer
	 * 
	 * @param tokenIdSet
	 *            the set we want to add to the collection of tokens of the customer
	 */
	public void addAllToken(Set<TokenId> tokenIdSet) {
		tokens.addAll(tokenIdSet);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bank_account_id == null) ? 0 : bank_account_id.hashCode());
		result = prime * result + ((cpr == null) ? 0 : cpr.hashCode());
		result = prime * result + ((first_name == null) ? 0 : first_name.hashCode());
		result = prime * result + ((last_name == null) ? 0 : last_name.hashCode());
		result = prime * result + ((tokens == null) ? 0 : tokens.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (bank_account_id == null) {
			if (other.bank_account_id != null)
				return false;
		} else if (!bank_account_id.equals(other.bank_account_id))
			return false;
		if (cpr == null) {
			if (other.cpr != null)
				return false;
		} else if (!cpr.equals(other.cpr))
			return false;
		if (first_name == null) {
			if (other.first_name != null)
				return false;
		} else if (!first_name.equals(other.first_name))
			return false;
		if (last_name == null) {
			if (other.last_name != null)
				return false;
		} else if (!last_name.equals(other.last_name))
			return false;
		if (tokens == null) {
			if (other.tokens != null)
				return false;
		} else if (!tokens.equals(other.tokens))
			return false;
		return true;
	}

}
