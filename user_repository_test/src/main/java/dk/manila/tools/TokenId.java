package dk.manila.tools;

import java.io.Serializable;

/**
 * Class representing an ID of a Token
 * 
 * @author Altug Tosun
 *
 */
public final class TokenId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2451533178565203311L;
	private final String token;

	/**
	 * Constructor of the TokenID
	 * 
	 * @param tokenID
	 *            ID of the token
	 */
	public TokenId(String tokenID) {
		this.token = tokenID;
	}

	/**
	 * Getter of the token ID
	 * 
	 * @return token ID
	 */
	public String get() {
		return token;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenId other = (TokenId) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

}
