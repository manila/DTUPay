package dk.manila.tools;

/**
 * Class representing a merchant in DTUPay
 * 
 * @author Altug Tosun
 *
 */
public final class Merchant {
	private final CprNumber cpr;
	private final String first_name;
	private final String last_name;
	private final String bank_account_id;

	/**
	 * Constructor of the class Merchant
	 * 
	 * @param cpr
	 *            the CprNumber of the customer
	 * @param firstname
	 *            the first name of the customer
	 * @param lastname
	 *            the last name of the customer
	 * @param bank_account_id
	 *            the identifier of the bank account
	 */
	public Merchant(CprNumber cpr, String firstname, String lastname, String bank_account_id) {
		this.cpr = cpr;
		this.first_name = firstname;
		this.last_name = lastname;
		this.bank_account_id = bank_account_id;
	}

	/**
	 * Getter of the first name of the customer
	 * 
	 * @return first name of the customer
	 */
	public String getFirstName() {
		return first_name;
	}

	/**
	 * Getter of the last name of the customer
	 * 
	 * @return last name of the customer
	 */
	public String getLastName() {
		return last_name;
	}

	/**
	 * Getter of the CPR of the customer
	 * 
	 * @return CPR of the customer
	 */
	public CprNumber getCpr() {
		return cpr;
	}

	/**
	 * Getter of the identifier of the bank account of the customer
	 * 
	 * @return identifier of the bank account of the customer
	 */
	public String getBankAccountId() {
		return bank_account_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bank_account_id == null) ? 0 : bank_account_id.hashCode());
		result = prime * result + ((cpr == null) ? 0 : cpr.hashCode());
		result = prime * result + ((first_name == null) ? 0 : first_name.hashCode());
		result = prime * result + ((last_name == null) ? 0 : last_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Merchant other = (Merchant) obj;
		if (bank_account_id == null) {
			if (other.bank_account_id != null)
				return false;
		} else if (!bank_account_id.equals(other.bank_account_id))
			return false;
		if (cpr == null) {
			if (other.cpr != null)
				return false;
		} else if (!cpr.equals(other.cpr))
			return false;
		if (first_name == null) {
			if (other.first_name != null)
				return false;
		} else if (!first_name.equals(other.first_name))
			return false;
		if (last_name == null) {
			if (other.last_name != null)
				return false;
		} else if (!last_name.equals(other.last_name))
			return false;
		return true;
	}

}
