package dk.manila.tools;

import java.io.Serializable;

/**
 * Class representing a CPR number
 * 
 * @author Alexandre Délèze
 *
 */
public final class CprNumber implements Serializable {
	private final String cpr;
	private static final long serialVersionUID = -425730408619464175L;

	/**
	 * Constructor of the class CprNumber
	 * 
	 * @param string
	 *            CPR Number
	 */
	public CprNumber(String string) {
		this.cpr = string;
	}

	/**
	 * Getter of the CPR Number
	 * 
	 * @return CPR Number
	 */
	public String get() {
		return cpr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpr == null) ? 0 : cpr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CprNumber other = (CprNumber) obj;
		if (cpr == null) {
			if (other.cpr != null)
				return false;
		} else if (!cpr.equals(other.cpr))
			return false;
		return true;
	}

}
