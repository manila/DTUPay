package dk.manila.tools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class ResponseObject implements Serializable {
	private static final long serialVersionUID = 3001973083766207814L;
	private final String operation;
	private List<Object> object;
	public ResponseObject(String operation, List<Object> object) {
		this.operation = operation;
		this.object = new ArrayList<>(object);
	}
	public String getOperation() {
		return operation;
	}
	public List<Object> getObject() {
		return object;
	}
}
