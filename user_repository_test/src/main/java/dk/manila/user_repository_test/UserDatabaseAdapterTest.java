package dk.manila.user_repository_test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.ResponseObject;

public class UserDatabaseAdapterTest {

	public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
		org.apache.log4j.BasicConfigurator.configure();
		Customer test = new Customer(new CprNumber("123456-1234"), "Daniel", "BigPain", "123456", new HashSet<>());
		ResponseObject response = new ResponseObject("registerCustomer", Arrays.asList(test));
		byte[] data = SerializationUtils.serialize(response);
		ConnectionFactory factory = new ConnectionFactory();
		String registerCustomerQueue = "registerCustomer";
		factory.setHost("rabbitmq");
		try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
			TimeUnit.SECONDS.sleep(2);
			// channel.queueDeclare(registerCustomerQueue, false, false, false, null);
			// channel.basicPublish("", registerCustomerQueue, null,data);
			String EXCHANGE_NAME = "queue";
			channel.exchangeDeclare(EXCHANGE_NAME, "direct");
			String routingKey = "fromAccountManagerToRepo";
			channel.basicPublish(EXCHANGE_NAME, routingKey, null, data);

			System.out.println(" [x] Sent");
		}
	}

}
