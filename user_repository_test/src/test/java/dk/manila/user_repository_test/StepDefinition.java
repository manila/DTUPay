package dk.manila.user_repository_test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.ResponseObject;
import dk.manila.tools.TokenId;
import javafx.util.Pair;

public class StepDefinition {
	private Channel channel;
	private String routingKey;
	private Customer customer;
	private TokenId tokenId;
	private final String EXCHANGE_NAME = "queue";
	@Given("^I create a channel with the queue$")
	public void i_create_a_channel_with_the_queue() throws Throwable {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("rabbitmq");
		Connection connection = factory.newConnection();
		channel = connection.createChannel();
    	channel.exchangeDeclare(EXCHANGE_NAME, "direct");
    	routingKey = "fromAccountManagerToRepo";
	}

	@Given("^I create a new customer with first name \"([^\"]*)\", last name \"([^\"]*)\", cpr number \"([^\"]*)\" and bank account id \"([^\"]*)\"$")
	public void i_create_a_new_customer_with_first_name_last_name_cpr_number_and_bank_account_id(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    customer = new Customer(new CprNumber(arg3), arg1, arg2, arg4, new HashSet<>());
	}

	@Given("^I create a new TokenId \"([^\"]*)\"$")
	public void i_create_a_new_TokenId(String arg1) throws Throwable {
	    tokenId = new TokenId(arg1);
	}

	@When("^I register the customer$")
	public void i_register_the_customer() throws Throwable {
		List<Object> arg = Arrays.asList(customer);
	    ResponseObject response = new ResponseObject("registerCustomer", arg);
	    byte[] data = SerializationUtils.serialize(response);
	    channel.basicPublish(EXCHANGE_NAME, routingKey, null,
                data);

	}

	@When("^I add him the new token$")
	public void i_add_him_the_new_token() throws Throwable {
		CprNumber cpr = customer.getCpr();
	    ResponseObject response = new ResponseObject("addTokenToCustomer",Arrays.asList(cpr,tokenId));
	    byte[] data = SerializationUtils.serialize(response);
	    channel.basicPublish(EXCHANGE_NAME, routingKey, null,
                data);
	}

	@When("^I ask to receive the customer's bank account id corresponding to the TokenId$")
	public void i_ask_to_receive_the_customer_s_bank_account_id_corresponding_to_the_TokenId() throws Throwable {
	    List<Object> arg = Arrays.asList(tokenId);
	}

	@Then("^I receive the right bank account id$")
	public void i_receive_the_right_bank_account_id() throws Throwable {
	    System.out.println("Finish");
	}



}
