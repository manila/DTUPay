package dk.manila.user_repository_test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "features", glue = "dk.manila.user_repository_test")
//@CucumberOptions(plugin = {"pretty"})
public class runTest {
}
