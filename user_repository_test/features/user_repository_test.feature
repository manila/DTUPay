#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Communicate with the user_repository

  Scenario: Get a bank account id
    Given I create a channel with the queue
    And I create a new customer with first name "Hubert", last name "Kind", cpr number "123456-7891" and bank account id "12345"
    And I create a new TokenId "125678"
    When I register the customer
    And I add him the new token
    And I ask to receive the customer's bank account id corresponding to the TokenId
    Then I receive the right bank account id

 