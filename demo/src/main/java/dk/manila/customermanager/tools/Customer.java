package dk.manila.customermanager.tools;

public class Customer {
	
	private final CprNumber cprNumber;
	private final String bankAccount_id;

	public Customer(CprNumber cpr, String bankAccount_id) {		
		this.cprNumber = cpr;
		this.bankAccount_id = bankAccount_id;
	}

	public CprNumber getCprNumber() {
		return cprNumber;
	}

	public String getBankAccount_id() {
		return bankAccount_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankAccount_id == null) ? 0 : bankAccount_id.hashCode());
		result = prime * result + ((cprNumber == null) ? 0 : cprNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (bankAccount_id == null) {
			if (other.bankAccount_id != null)
				return false;
		} else if (!bankAccount_id.equals(other.bankAccount_id))
			return false;
		if (cprNumber == null) {
			if (other.cprNumber != null)
				return false;
		} else if (!cprNumber.equals(other.cprNumber))
			return false;
		return true;
	}

}