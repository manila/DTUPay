package dk.manila.customermanager.repository;

import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

public interface CustomerRepository {

	void addCustomer(Customer customer);

	String getCustomerAccountId(CprNumber cpr);

	boolean contains(CprNumber cprNumber);

}
