package dk.manila.customermanager.repository;

import java.util.HashSet;
import java.util.Set;

import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

public final class CustomerRepositoryImpl implements CustomerRepository {
	public static final CustomerRepositoryImpl INSTANCE_CUSTOMER_REPOSITORY = new CustomerRepositoryImpl();
	private CustomerRepositoryImpl() {

	}
	private final Set<Customer> customers = new HashSet<>();

	public Customer getCustomerByCpr(CprNumber cpr) {

		for (Customer c : customers) {
			if (c.getCprNumber().equals(cpr)) {

				return c;
			}

		}

		return null;
	}

	@Override
	public void addCustomer(Customer customer) {
		if(customer==null)
			throw new NullPointerException();
		customers.add(customer);
	}

	@Override
	public String getCustomerAccountId(CprNumber cpr) {
		for(Customer c:customers) {
			if(c.getCprNumber().equals(cpr))
				return c.getBankAccount_id();
		}
		return null;
	}

	@Override
	public boolean contains(CprNumber cprNumber) {
		for(Customer c:customers)
			if(c.getCprNumber().equals(cprNumber))
				return true;
		return false;
	}

}
