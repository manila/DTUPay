package dk.manila.customermanager.rest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.customermanager.queue.CustomerQueue;
import dk.manila.customermanager.queue.CustomerQueueImpl;

@ApplicationPath("/")
public class RestApplication extends Application {

	public RestApplication() throws IOException, TimeoutException {
		CustomerQueue queue = new CustomerQueueImpl();
		queue.execute("listener", null);
		

	}
}
