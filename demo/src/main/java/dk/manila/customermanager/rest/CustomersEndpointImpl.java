package dk.manila.customermanager.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.customermanager.customerservice.CustomerService;
import dk.manila.customermanager.customerservice.CustomerServiceImpl;
import dk.manila.customermanager.queue.CustomerQueue;
import dk.manila.customermanager.queue.CustomerQueueImpl;
import dk.manila.customermanager.repository.CustomerRepositoryImpl;

//
@Path("/customers")
public class CustomersEndpointImpl {
	private CustomerQueue queue;
	private CustomerService service;
	public CustomersEndpointImpl() throws IOException, TimeoutException {
		queue = new CustomerQueueImpl();
		service = new CustomerServiceImpl(CustomerRepositoryImpl.INSTANCE_CUSTOMER_REPOSITORY, queue);
	}

	@POST
	@Path("")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response createCustomer(String customer) {
		if(service.contains(customer))
			return Response.status(400).entity(customer + "is already registered").build();
		ArrayList<Object> arg = new ArrayList<>();
		arg.add(customer);
		try {
			queue.execute("get_account_id_by_cpr", arg);
		} catch (IOException | TimeoutException e) {
			return Response.status(500).build();
		}

		return Response.ok().entity(customer).build();

	}

}
