package dk.manila.customermanager.queue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public interface CustomerQueue {
	public void execute(String instruction, ArrayList<Object> arg) throws IOException, TimeoutException;
}
