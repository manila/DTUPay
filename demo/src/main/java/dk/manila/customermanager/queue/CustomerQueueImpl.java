package dk.manila.customermanager.queue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import dk.manila.customermanager.customerservice.CustomerService;
import dk.manila.customermanager.customerservice.CustomerServiceImpl;
import dk.manila.customermanager.repository.CustomerRepositoryImpl;

public class CustomerQueueImpl implements CustomerQueue {
	private final Connection connection;
	private final String EXCHANGE_NAME = "dtu_pay_exchange";
	private final String GET_ACCOUNT_ID = "get_account_id_by_cpr_response";
	private final String NEW_CUSTOMER_REGISTRATION = "register_customer_request";
	private final CustomerService service;

	public CustomerQueueImpl() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("rabbitmq");
		connection = factory.newConnection();
		service = new CustomerServiceImpl(CustomerRepositoryImpl.INSTANCE_CUSTOMER_REPOSITORY,this);

	}

	public void execute(String instruction, ArrayList<Object> arg) throws IOException, TimeoutException {
		byte[] data;
		switch (instruction) {
		case "listener":
			Channel channelToPayment = connection.createChannel();
			String queueName = channelToPayment.queueDeclare().getQueue();
			channelToPayment.exchangeDeclare(EXCHANGE_NAME, "fanout");
			channelToPayment.queueBind(queueName, EXCHANGE_NAME, GET_ACCOUNT_ID);
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
				List<Object> response = SerializationUtils.deserialize(delivery.getBody());
				String cpr = (String) response.get(0);
				String bankAccountId = (String) response.get(1);
				try {
					service.registerCustomer(cpr, bankAccountId);
				} catch (TimeoutException e) {
					e.printStackTrace();
				}
			};
			channelToPayment.basicConsume(queueName, true, deliverCallback, consumerTag -> {
			});
			break;
		case "new_customer_notification":
			Channel channelToToken = connection.createChannel();
			channelToToken.exchangeDeclare(EXCHANGE_NAME, "fanout");
			data = SerializationUtils.serialize(arg);
			channelToToken.basicPublish(EXCHANGE_NAME, NEW_CUSTOMER_REGISTRATION, null, data);
			channelToToken.close();
			break;
		case "get_account_id_by_cpr":
			Channel channelToPaymentSend = connection.createChannel();
			channelToPaymentSend.exchangeDeclare(EXCHANGE_NAME, "fanout");
			data = SerializationUtils.serialize(arg);
			channelToPaymentSend.basicPublish(EXCHANGE_NAME, GET_ACCOUNT_ID, null, data);
			channelToPaymentSend.close();
			break;
			
		}

	}
}
