package dk.manila.customermanager.customerservice;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface CustomerService {

	void registerCustomer(String cpr, String bankAccountId) throws IOException, TimeoutException;

	String getCustomerAccountId(String cpr);
	
	boolean contains(String cpr);
}
