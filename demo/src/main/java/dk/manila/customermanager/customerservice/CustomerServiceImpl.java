package dk.manila.customermanager.customerservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import dk.manila.customermanager.queue.CustomerQueue;
import dk.manila.customermanager.repository.CustomerRepository;
import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;
	private final CustomerQueue queue;
	
	
	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerQueue queue) throws IOException, TimeoutException {
		this.customerRepository = customerRepository;
		this.queue = queue;
	}
	

	@Override
	public String getCustomerAccountId(String cpr) {
		return this.customerRepository.getCustomerAccountId(new CprNumber(cpr));
	}


	@Override
	public void registerCustomer(String cpr, String bankAccountId) throws IOException, TimeoutException {
		CprNumber cprNumber = new CprNumber(cpr);
		Customer customer = new Customer(cprNumber, bankAccountId);
		customerRepository.addCustomer(customer);
		ArrayList<Object> arg = new ArrayList<>();
		arg.add(cpr);
		queue.execute("new_customer_notification",arg);
	}


	@Override
	public boolean contains(String cpr) {
		CprNumber cprNumber = new CprNumber(cpr);
		return customerRepository.contains(cprNumber);
	}
	


	
}
