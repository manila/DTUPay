package dk.manila.customermanager.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

public class CustomerRepositoryTest {
	private CustomerRepository repository;
	private Customer customer;

	@Before
	public void initialisation() {
		customer = new Customer(new CprNumber("123456-7891"), "1234");
		repository = CustomerRepositoryImpl.INSTANCE_CUSTOMER_REPOSITORY;
	}

	@Test
	public void add_a_new_customer() {
		repository.addCustomer(customer);
		assertTrue(repository.contains(customer.getCprNumber()));
	}

	@Test(expected = NullPointerException.class)
	public void add_a_null_customer_and_receive_null_exception() {
		repository.addCustomer(null);
	}
	
	@Test
	public void add_a_customer_and_check_if_the_set_contains_it() {
		repository.addCustomer(customer);
		assertTrue(repository.contains(customer.getCprNumber()));
	}
	
	@Test
	public void contains_with_a_nonregistered_customer() {
		assertFalse(repository.contains(customer.getCprNumber()));
	}
	@Test
	public void getCustomerAccountId_with_a_register_customer() {
		repository.addCustomer(customer);
		assertEquals(customer.getBankAccount_id(), repository.getCustomerAccountId(customer.getCprNumber()));
	}
}
