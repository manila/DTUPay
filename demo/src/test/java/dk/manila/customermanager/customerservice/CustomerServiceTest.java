package dk.manila.customermanager.customerservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;

import dk.manila.customermanager.customerservice.CustomerService;
import dk.manila.customermanager.customerservice.CustomerServiceImpl;
import dk.manila.customermanager.queue.CustomerQueue;
import dk.manila.customermanager.repository.CustomerRepository;
import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;



public class CustomerServiceTest {
	private CustomerRepository repository;
	private CustomerQueue queue;
	private CustomerService service;
	private String cpr;
	private String bankAccountId;
	@Before
	public void initialisation() throws IOException, TimeoutException {
		repository = new CustomerRepository() {
			private Customer customer;
			@Override
			public String getCustomerAccountId(CprNumber cpr) {
				// TODO Auto-generated method stub
				return customer.getBankAccount_id();
			}
			
			@Override
			public void addCustomer(Customer customer) {
				this.customer = customer;
				
			}

			@Override
			public boolean contains(CprNumber cprNumber) {
				// TODO Auto-generated method stuƒ
				if(customer!=null)
					return customer.getCprNumber().equals(cprNumber);
				return false;
			}
		};
		queue = new CustomerQueue() {
			
			@Override
			public void execute(String instruction, ArrayList<Object> arg) throws IOException, TimeoutException {
				// TODO Auto-generated method stub
				
			}
		};
		service = new CustomerServiceImpl(repository, queue);
		cpr = "123456-7891";
		bankAccountId = "1234";
		
	}
	@Test
	public void add_new_customer_to_repository() throws IOException, TimeoutException {
		service.registerCustomer(cpr, bankAccountId);
		assertEquals(bankAccountId, service.getCustomerAccountId(cpr));
	}
	
	@Test
	public void contains_cpr_with_a_registerCustomer() throws IOException, TimeoutException {
		service.registerCustomer(cpr, bankAccountId);
		assertTrue(service.contains(cpr));
	}
	@Test
	public void contains_with_a_non_registered_cpr() {
		assertFalse(service.contains(cpr));
	}
	
	
}
