package dk.manila.paymentmanager.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class PaymentResource {
	@XmlElement public String payerTokenId;
	@XmlElement public String payeeCpr;
	@XmlElement public String amount;		
	
	/*
	@XmlElement(name="payerTokenId")
	public String getPayerTokenId() {
		return payerTokenId;
	}		
	@XmlElement(name="payeeCpr")
	public String getPayeeCpr() {
		return payeeCpr;
	}	
	@XmlElement(name="amount")
	public String getAmount() {
		return amount;
	}*/
}
