package dk.manila.paymentmanager.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;


import dk.manila.paymentmanager.resources.PaymentResource;

import javax.ws.rs.core.MediaType;

@Path("/payments")
public class PaymentEndpointImpl {
	
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createPayment(String payerTokenId){
		return Response.ok(payerTokenId).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON) //@Produces(MediaType.TEXT_PLAIN)
	public Response createPayment(PaymentResource paymentRequest) {
		//return Response.ok(paymentRequest.amount + paymentRequest.payeeCpr + paymentRequest.payerTokenId).build();
		return Response.ok(Entity.entity(paymentRequest, MediaType.APPLICATION_JSON)).build();
	}
}
