package dk.manila.user_repository.tools;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.manila.tools.CprNumber;

public class CPRTest {
	private CprNumber test1;
	private CprNumber test2;
	private CprNumber test3;
	private String cpr1;
	private String cpr3;
	@Before
	public void initialization_test() {
		cpr1 = "123456-7891";
		test1 = new CprNumber(cpr1);
		test2 = new CprNumber(cpr1);
		cpr3 = "234567-8912";
		test3 = new CprNumber(cpr3);
		
	}
	@Test
	public void getCprTest() {
		assertEquals(cpr1, test1.get());
	}
	@Test
	public void equals_for_equals_objects_Test() {
		assertTrue(test1.equals(test2));
	}
	@Test
	public void hashCode_for_equals_objects_Test() {
		assertEquals(test1.hashCode(),test2.hashCode());
	}
	@Test
	public void equals_for_different_objects() {
		assertFalse(test1.equals(test3));
	}
	@Test
	public void hashCode_for_different_objects_Test() {
		assertFalse(test1.hashCode()==test3.hashCode());
	}

}
