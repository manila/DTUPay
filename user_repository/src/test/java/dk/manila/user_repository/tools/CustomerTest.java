package dk.manila.user_repository.tools;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.TokenId;

public class CustomerTest {

	private Customer customer1;
	private Customer customer2;
	private Customer customer3;
	private String firstname1;
	private String lastname1;
	private CprNumber cpr1;
	private String bank_account_id1;
	private String firstname3;
	private String lastname3;
	private CprNumber cpr3;
	private String bank_account_id3;
	private Set<TokenId> set1;
	private Set<TokenId> set3;
	@Before
	public void initialization_test() {
		firstname1 = "Hubert";
		lastname1 = "Suffer";
		cpr1 = new CprNumber("123456-7891");
		bank_account_id1 = "23456789";
		set1 = new HashSet<>();
		customer1 = new Customer(cpr1,firstname1,lastname1,bank_account_id1,set1);
		customer2 = new Customer(cpr1,firstname1,lastname1,bank_account_id1,set1);
		firstname3 = "Sophia";
		lastname3 = "Suffer";
		cpr3 = new CprNumber("234567-8912");
		bank_account_id3 = "12345678";
		set3 = new HashSet<>();
		customer3 = new Customer(cpr3, firstname3, lastname3, bank_account_id3,set3);
	}
	@Test
	public void get_first_name_test() {
		assertEquals(firstname1, customer1.getFirstName());
	}
	@Test
	public void get_last_name_test() {
		assertEquals(lastname1, customer1.getLastName());
	}
	@Test
	public void get_cpr_test() {
		assertTrue(customer1.getCpr().equals(cpr1));
	}
	
	@Test
	public void get_bank_account_id_test() {
		assertEquals(bank_account_id1, customer1.getBankAccountId());
	}
	@Test
	public void get_tokenSet_test() {
		assertTrue(customer1.getTokenSet().equals(set1));
	}
	@Test
	public void add_one_token_set_Test() {
		set1.add(new TokenId("123456-1234"));
		customer1.addToken(new TokenId("123456-1234"));
		assertTrue(set1.equals(customer1.getTokenSet()));
	}
	@Test
	public void addAll_tokenSet_Test(){
		Set<TokenId> tokenIdSet = new HashSet<>();
		tokenIdSet.add(new TokenId("109375-5567"));
		tokenIdSet.add(new TokenId("567392-6839"));
		set1.addAll(tokenIdSet);
		customer1.addAllToken(tokenIdSet);
		assertTrue(set1.equals(customer1.getTokenSet()));
	}
	
	@Test
	public void equals_for_equals_objects_Test() {
		assertTrue(customer1.equals(customer2));
	}
	@Test
	public void hashCode_for_equals_objects_Test() {
		assertEquals(customer1.hashCode(),customer2.hashCode());
	}
	@Test
	public void equals_for_different_objects() {
		assertFalse(customer1.equals(customer3));
	}
	@Test
	public void hashCode_for_different_objects_Test() {
		assertFalse(customer1.hashCode()==customer3.hashCode());
	}



}
