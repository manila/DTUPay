package dk.manila.user_repository.tools;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.manila.tools.TokenId;

public class TokenIdTest {
	private TokenId test1;
	private TokenId test2;
	private TokenId test3;
	private String token1;
	private String token3;

	@Before
	public void initialization_test() {
		token1 = "123456-7891";
		test1 = new TokenId(token1);
		test2 = new TokenId(token1);
		token3 = "234567-8912";
		test3 = new TokenId(token3);

	}

	@Test
	public void getCprTest() {
		assertEquals(token1, test1.get());
	}

	@Test
	public void equals_for_equals_objects_Test() {
		assertTrue(test1.equals(test2));
	}

	@Test
	public void hashCode_for_equals_objects_Test() {
		assertEquals(test1.hashCode(), test2.hashCode());
	}

	@Test
	public void equals_for_different_objects() {
		assertFalse(test1.equals(test3));
	}

	@Test
	public void hashCode_for_different_objects_Test() {
		assertFalse(test1.hashCode() == test3.hashCode());
	}

}
