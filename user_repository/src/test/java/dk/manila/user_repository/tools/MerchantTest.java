package dk.manila.user_repository.tools;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Merchant;

public class MerchantTest {
	private Merchant merchant1;
	private Merchant merchant2;
	private Merchant merchant3;
	private String firstname1;
	private String lastname1;
	private CprNumber cpr1;
	private String bank_account_id1;
	private String firstname3;
	private String lastname3;
	private CprNumber cpr3;
	private String bank_account_id3;
	@Before
	public void initialization_test() {
		firstname1 = "Hubert";
		lastname1 = "Suffer";
		cpr1 = new CprNumber("123456-7891");
		bank_account_id1 = "23456789";
		merchant1 = new Merchant(cpr1,firstname1,lastname1,bank_account_id1);
		merchant2 = new Merchant(cpr1,firstname1,lastname1,bank_account_id1);
		firstname3 = "Sophia";
		lastname3 = "Suffer";
		cpr3 = new CprNumber("234567-8912");
		bank_account_id3 = "12345678";
		merchant3 = new Merchant(cpr3, firstname3, lastname3, bank_account_id3);
	}
	@Test
	public void get_first_name_test() {
		assertEquals(firstname1, merchant1.getFirstName());
	}
	@Test
	public void get_last_name_test() {
		assertEquals(lastname1, merchant1.getLastName());
	}
	@Test
	public void get_cpr_test() {
		assertTrue(merchant1.getCpr().equals(cpr1));
	}
	
	@Test
	public void get_bank_account_id_test() {
		assertEquals(bank_account_id1, merchant1.getBankAccountId());
	}
	@Test
	public void equals_for_equals_objects_Test() {
		assertTrue(merchant1.equals(merchant2));
	}
	@Test
	public void hashCode_for_equals_objects_Test() {
		assertEquals(merchant1.hashCode(),merchant2.hashCode());
	}
	@Test
	public void equals_for_different_objects() {
		assertFalse(merchant1.equals(merchant3));
	}
	@Test
	public void hashCode_for_different_objects_Test() {
		assertFalse(merchant1.hashCode()==merchant3.hashCode());
	}


}
