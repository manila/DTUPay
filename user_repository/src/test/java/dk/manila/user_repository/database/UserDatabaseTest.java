package dk.manila.user_repository.database;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.Merchant;
import dk.manila.tools.TokenId;

public class UserDatabaseTest {

	private UserDatabase database1;
	private Merchant merchant1;
	private Customer customer1;

	@Before
	public void initilization_test() {

		database1 = new UserDatabase();
		customer1 = new Customer(new CprNumber("23456789-1212"), "Manila", "Suffer", "234567", new HashSet<TokenId>());
		merchant1 = new Merchant(new CprNumber("12345678-1212"), "Hubert", "Suffer", "123456");
	}

	@Test
	public void register_customer_test() {

		database1.registerCustomer(customer1);
		assertEquals(customer1, database1.getCustomer(new CprNumber("23456789-1212")));
	}

	@Test
	public void register_merchant_test() {
		database1.registerMerchant(merchant1);
		assertEquals(merchant1, database1.getMerchant(new CprNumber("12345678-1212")));
	}

	@Test
	public void add_token_to_Right_Customer_test() {

		TokenId token1 = new TokenId("12345678");
		database1.registerCustomer(customer1);
		database1.addTokentoCustomer(new CprNumber("23456789-1212"), token1);
		assertEquals(customer1.getBankAccountId(), database1.getAccountId(token1));
	}

	@Test
	public void get_AccountId_Merchant() {

		database1.registerMerchant(merchant1);
		assertEquals(merchant1.getBankAccountId(), database1.getAccountId(merchant1.getCpr()));

	}

}
