package dk.manila.user_repository.database;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.Merchant;
import dk.manila.tools.TokenId;

public interface UserDatabaseInterface {

	public void registerCustomer(Customer customer1);

	public Customer getCustomer(CprNumber cprNumber);

	public void registerMerchant(Merchant merchant1);

	public Merchant getMerchant(CprNumber cprNumber);

	public void addTokentoCustomer(CprNumber cprNumber, TokenId token1);

	public String getAccountId(TokenId token1);

	public String getAccountId(CprNumber cpr);
}
