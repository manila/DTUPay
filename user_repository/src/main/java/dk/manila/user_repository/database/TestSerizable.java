package dk.manila.user_repository.database;

import java.util.HashSet;

import org.apache.commons.lang3.SerializationUtils;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;

public class TestSerizable {
	public static void main(String[] args) {
		Customer test = new Customer(new CprNumber("12"), "Hubert", "Pain", "122334", new HashSet<>());
		byte[] data = SerializationUtils.serialize(test);
		Customer answer = SerializationUtils.deserialize(data);
		System.out.println(answer.getCpr().get());
	}
}
