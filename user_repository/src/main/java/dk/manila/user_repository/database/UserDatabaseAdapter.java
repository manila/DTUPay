package dk.manila.user_repository.database;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.BasicConfigurator;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import dk.manila.tools.*;
import javafx.util.Pair;

public class UserDatabaseAdapter {
	private final static UserDatabaseInterface database = new UserDatabase();

	public void doSomething() {

	}

	public static void main(String[] args) throws IOException, TimeoutException {
		//BasicConfigurator.configure();
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("rabbitmq");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		// String newCustomerQueue = "registerCustomer";
		// channel.queueDeclare(newCustomerQueue, false, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		// DeliverCallback deliverCallback = (consumerTag, delivery) -> {
		// byte[] data = delivery.getBody();
		// Customer test = SerializationUtils.deserialize(data);
		// System.out.println(test.getFirstName());
		// };
		// channel.basicConsume(newCustomerQueue, true, deliverCallback, consumerTag ->
		// {
		// });
		String EXCHANGE_NAME = "queue";
		channel.exchangeDeclare(EXCHANGE_NAME, "direct");
		String queueName = channel.queueDeclare().getQueue();
		String routingKey = "fromAccountManagerToRepo";
		channel.queueBind(queueName, EXCHANGE_NAME, routingKey);
		/* channel.queueBind(queueName, EXCHANGE_NAME, "rk2"); */
		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			ResponseObject response = SerializationUtils.deserialize(delivery.getBody());
			switch (response.getOperation()) {
			case "registerCustomer":
				Customer c = (Customer) response.getObject().get(0);
				CprNumber cpr = c.getCpr();
				database.registerCustomer(c);
				System.out.println(database.getCustomer(cpr).getFirstName());
				break;

			case "addTokenToCustomer":
				List<Object> arg = (List<Object>) response.getObject();
				database.addTokentoCustomer((CprNumber) arg.get(0), (TokenId) arg.get(1));
				String tmp = database.getAccountId((TokenId) arg.get(1));
				System.out.println(tmp);
				break;
			}

		};
		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
		});
	}

}
