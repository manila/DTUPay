package dk.manila.user_repository.database;

import java.util.HashSet;
import java.util.Set;

import dk.manila.tools.CprNumber;
import dk.manila.tools.Customer;
import dk.manila.tools.Merchant;
import dk.manila.tools.TokenId;

public class UserDatabase implements UserDatabaseInterface {

	private final Set<Customer> customers = new HashSet<Customer>();
	private final Set<Merchant> merchants = new HashSet<Merchant>();

	public void registerCustomer(Customer customer1) {

		customers.add(customer1);

	}

	public Customer getCustomer(CprNumber cprNumber) {

		for (Customer customer : customers) {

			if (customer.getCpr().equals(cprNumber)) {
				return customer;
			}
		}

		return null;

	}

	public void registerMerchant(Merchant merchant1) {
		merchants.add(merchant1);

	}

	public Merchant getMerchant(CprNumber cprNumber) {

		for (Merchant merchant : merchants) {

			if (merchant.getCpr().equals(cprNumber)) {
				return merchant;
			}
		}

		return null;
	}

	public void addTokentoCustomer(CprNumber cprNumber, TokenId token1) {

		for (Customer customer : customers) {

			if (customer.getCpr().equals(cprNumber)) {

				customer.addToken(token1);
			}
		}

	}

	public String getAccountId(TokenId token1) {

		for (Customer customer : customers) {

			if (customer.getTokenSet().contains(token1)) {

				return customer.getBankAccountId();
			}
		}
		return null;
	}

	public String getAccountId(CprNumber cpr) {

		for (Merchant merchant : merchants) {

			if (merchant.getCpr().equals(cpr)) {

				return merchant.getBankAccountId();
			}
		}

		return null;
	}

}
