package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

public class HelloWorldEndpointImplTest {
	
	@Test
	public void givenGetHello_returnHelloString() {		
		Client client = ClientBuilder.newClient();		
		WebTarget webTarget = client.target("http://02267-manila.compute.dtu.dk:8484");
		webTarget = webTarget.path("hello");
		
		Response response = webTarget.request().get();		
		assertEquals(200, response.getStatus());
		assertEquals("hello", response.readEntity(String.class));
	}
	
	
	/*
	@Test
	public void postRequestTokenUnregisteredCustomerTest() {
		Response response = sim.postRequestToken();
		
		String message = "";
		try {
			JSONObject json = new JSONObject(response.readEntity(String.class));
			message = (String) json.get("message");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		assertEquals(403, response.getStatus());
		assertEquals("CPR number not registered as a customer", message);
	}*/
	

}