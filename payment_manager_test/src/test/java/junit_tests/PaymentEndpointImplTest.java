package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import resources.PaymentResource;

public class PaymentEndpointImplTest {
	
	@Test
	public void givenPaymentPostWithTokenId_returnPostWasSuccessful() {
		Client client = ClientBuilder.newClient();		
		WebTarget webTarget = client.target("http://02267-manila.compute.dtu.dk:8484");
		webTarget = webTarget.path("payments");
		
		Response response = webTarget.request().post(Entity.entity("0123456789", MediaType.TEXT_PLAIN));		
		assertEquals(200, response.getStatus());
		assertEquals("0123456789", response.readEntity(String.class));
	}
	
	
	@Test
	public void givenPaymentPostWithPayerTokenIdAndPayeeCprAndAmount_returnPostWasSuccessful() {
		Client client = ClientBuilder.newClient();		
		WebTarget webTarget = client.target("http://02267-manila.compute.dtu.dk:8484");
		webTarget = webTarget.path("payments");
		
		PaymentResource paymentRequest = new PaymentResource();
		paymentRequest.payerTokenId = "0123456789";
		paymentRequest.payeeCpr = "121282-2266";
		paymentRequest.amount = "22000.99";
		
		
		Response response = webTarget.request().post(Entity.entity(paymentRequest, MediaType.APPLICATION_JSON));		
		/*
		assertEquals(200, response.getStatus());
		assertEquals("0123456789", response.readEntity(String.class));
		*/
		assertEquals(200, response.getStatus());
		
		/*
		PaymentResource paymentResponse = response.readEntity(PaymentResource.class);
		assertEquals(paymentRequest.payerTokenId, paymentResponse.payerTokenId);
		assertEquals(paymentRequest.payeeCpr, paymentResponse.payeeCpr);
		assertEquals(paymentRequest.amount, paymentResponse.amount);
		*/
	}
	
	
	
}
