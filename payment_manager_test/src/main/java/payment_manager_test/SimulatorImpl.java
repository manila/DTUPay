package payment_manager_test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

public class SimulatorImpl {

	WebTarget webTarget;
	
	public SimulatorImpl() {
		Client client = ClientBuilder.newClient();
		
		webTarget = client.target("http://02267-manila.compute.dtu.dk:8484");
		webTarget = webTarget.path("tokens");
	}
	
	
	
	
	public Response postRequestToken() {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("quantity", 3);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Response response = webTarget.request()
				.header("Authorization", "Bearer 0123456789")
				.post(Entity.json(json.toString()));
		
		return response;
	}
}