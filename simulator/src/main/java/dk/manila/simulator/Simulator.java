package dk.manila.simulator;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

public class Simulator {

	public static void main(String[] args) {
		
		Client client = ClientBuilder.newClient();
		
		WebTarget postTarget = client.target("http://localhost:8080");
		WebTarget resourcePostTarget = postTarget.path("tokens");
		
		JSONObject json = new JSONObject();
		try {
			json.put("quantity", 4);
			json.put("amounts", new double[] { 3, 10, 20.5 });
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Response postResponse = resourcePostTarget.request().post(Entity.json(json.toString()));
		
		System.out.println(postResponse.getStatus());
		System.out.println(postResponse.readEntity(String.class));
		
		
		WebTarget webTarget = client.target("http://localhost:8080/");
		//WebTarget resourceWebTarget = webTarget.path("tokens/7026d380-ec27-4dd7-974a-7bd91830aed4");
		WebTarget resourceWebTarget = webTarget.path("tokens");
		
		Invocation.Builder invocationBuilder = resourceWebTarget.request(MediaType.APPLICATION_JSON);
		//invocationBuilder.header("some-header", "true");
		 
		Response response = invocationBuilder.get();
		System.out.println(response.getStatus());
		System.out.println(response.readEntity(String.class));
		
	}

}
