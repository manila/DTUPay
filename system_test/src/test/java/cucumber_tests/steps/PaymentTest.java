package cucumber_tests.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;

public class PaymentTest {
	final String CUSTOMER_MANAGER_HOSTNAME = "http://02267-manila.compute.dtu.dk:9393/"; 
	final String MERCHANT_MANAGER_HOSTNAME = "http://02267-manila.compute.dtu.dk:8686/"; 
	final String TOKEN_MANAGER_HOSTNAME = "http://02267-manila.compute.dtu.dk:8383/"; 
	final String cprCustomer = "0303992266";
	final String cprMerchant = "2903789076";
	String responseToken;
	
	@Given("^A registered customer with a bank account$")
	public void a_registered_customer_with_a_bank_account() throws Exception {
		
	   Response responseCustomer = createCustomer(cprCustomer);	    	   
	   assertEquals(200,responseCustomer.getStatus());
	      
	   BankService bank = new BankServiceService().getBankServicePort();
	   
	   //BankServiceException_Exception: Account does not exist	   
	   Account accountCustomer = bank.getAccountByCprNumber(cprCustomer);
	   String accountIdCustomer = accountCustomer.getId();
	   //assertNotNull(accountIdCustomer);
		//throw new PendingException();
	}

	private Response createCustomer(String cprNumber) {
		Client client = ClientBuilder.newClient();
	    WebTarget customerManager = client.target(CUSTOMER_MANAGER_HOSTNAME)
	    								  .path("customers");	    
	    //Invocation.Builder invocationBuilder = customerManager.request(MediaType.APPLICATION_JSON);
//    JSONObject clientCprNumber = new JSONObject("{\"cprNumber\":\"0303992266\"}");
	    //Response response = customerManager.request().post(Entity.entity(clientCprNumber.toString(), MediaType.APPLICATION_JSON));
	    //Response response = customerManager.request().post(Entity.text("0303992266"));
	    //.queryParam("cpr", "0303992266")
	    Response responseCustomer = customerManager.request().post(Entity.entity(cprNumber, MediaType.TEXT_PLAIN));
	    return responseCustomer;
	}

	
	@Given("^A registered merchant with a bank account$")
	public void a_registered_merchant_with_a_bank_account() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
//		   Response responseMerchant = createMerchant(cprMerchant);	    	   
//		   assertEquals(200,responseMerchant.getStatus());
//		      
//		   BankService bank = new BankServiceService().getBankServicePort();
//		   
//		   //BankServiceException_Exception: Account does not exist	   
//		   Account accountMerchant = bank.getAccountByCprNumber(cprMerchant);
//		   String accountIdMerchant = accountMerchant.getId();
//		   assertNotNull(accountIdMerchant);
	    throw new PendingException();
	}

	private Response createMerchant(String cprNumber) {
		Client client = ClientBuilder.newClient();
	    WebTarget merchantManager = client.target(MERCHANT_MANAGER_HOSTNAME)
	    								  .path("hello");	    
	    //Invocation.Builder invocationBuilder = customerManager.request(MediaType.APPLICATION_JSON);
//    JSONObject clientCprNumber = new JSONObject("{\"cprNumber\":\"0303992266\"}");
	    //Response response = customerManager.request().post(Entity.entity(clientCprNumber.toString(), MediaType.APPLICATION_JSON));
	    //Response response = customerManager.request().post(Entity.text("0303992266"));
	    //.queryParam("cpr", "0303992266")
	    Response responseMerchant = merchantManager.request().post(Entity.entity(cprNumber, MediaType.TEXT_PLAIN));
	    return responseMerchant;
	}
	
	@Given("^The customer has one unused token$")
	public void the_customer_has_one_unused_token() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
//		responseToken = checkTokens();	
		
	    throw new PendingException();
	}
	
	private String checkTokens() {
		Client client = ClientBuilder.newClient();
	    WebTarget tokenManager = client.target(TOKEN_MANAGER_HOSTNAME)
	    								  .path("hello");	    
	    String response = tokenManager.request().get(String.class);
	    return response;
	}

	@When("^The merchant scans the token$")
	public void the_merchant_scans_the_token() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		//assertEquals("Hello from Thorntail!",responseToken);
	    throw new PendingException();
	}

	@When("^Requests payment for (\\d+) kr using the token$")
	public void requests_payment_for_kr_using_the_token(int arg1) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^The payment succeeds$")
	public void the_payment_succeeds() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^The money is transferred from the customer bank account to the merchant bank account$")
	public void the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	
}
