Feature: Payment
      
	Scenario: Simple payment
		Given A registered customer with a bank account
		And A registered merchant with a bank account
		And The customer has one unused token
		When The merchant scans the token
		And Requests payment for 100 kr using the token
		Then The payment succeeds
		And The money is transferred from the customer bank account to the merchant bank account   
		
	