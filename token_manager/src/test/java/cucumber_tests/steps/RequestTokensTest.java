package cucumber_tests.steps;

import static org.junit.Assert.assertEquals;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.model.TokenRepresentation;

public class RequestTokensTest {
	private CprNumber cpr;
	private int tokensLeft, tokensGenerated;
	private String errorMessage;
	
	private TokenId tokenId;
	private List<TokenRepresentation> tokens;
	private TokenRepresentation token;
	
	private TokenService tokenService;
	private TokenRepository tokenRepository;
	
	public RequestTokensTest() {
		this.tokenRepository = new TokenRepositoryImpl();
		this.tokenService = new TokenServiceImpl(tokenRepository);
	}
	
	@Given("^A registered customer with CPR number \"([^\"]*)\"$")
	public void a_registered_customer_with_CPR_number(String cpr) throws Exception {
	    try {
	    	this.cpr = new CprNumber(cpr);
	    	this.tokenService.addCustomer(this.cpr);
	    } catch (CprException e) {
	    	this.errorMessage = e.getMessage();
	    }
	}
	
	@Given("^A unregistered customer with CPR number \"([^\"]*)\"$")
	public void a_unregistered_customer_with_CPR_number(String cpr) throws Exception {
	    // Do not register
	}

	@Given("^The customer has (\\d+) unused token\\(s\\) left$")
	public void the_customer_has_unused_token_s_left(int tokensLeft) throws Exception {
		try {
			this.tokensLeft = tokenService.requestTokens(cpr, tokensLeft).size();
		} catch (CprException e) {
	    	this.errorMessage = e.getMessage();
		} catch (TokenException e) {
	    	this.errorMessage = e.getMessage();
		}
	}

	@When("^The customer requests (\\d+) token\\(s\\)$")
	public void the_customer_requests_token_s(int tokensRequested) throws Exception {
		try {
			this.tokensGenerated = tokenService.requestTokens(cpr, tokensRequested).size();
		} catch (CprException e) {
	    	this.errorMessage = e.getMessage();
		} catch (TokenException e) {
	    	this.errorMessage = e.getMessage();
		}
	}

	@Then("^The customer gets back (\\d+) token\\(s\\)$")
	public void the_customer_gets_back_token_s(int tokensReceived) throws Exception {
	    assertEquals(tokensReceived, tokensGenerated);
	}

	@Then("^The customer has (\\d+) unused token\\(s\\) in total$")
	public void the_customer_has_unused_token_s_in_total(int tokensLeftInTotal) throws Exception {
	    assertEquals(tokensLeftInTotal, tokenService.getUnusedTokens(cpr).size());
	}
	
	@Then("^The customer gets back an error message \"([^\"]*)\"$")
	public void the_customer_gets_back_an_error_message(String errMsg) throws Exception {
		assertEquals(errMsg, errorMessage);
	}

	@Given("^the customer has at least one unused token")
	public void the_customer_has_at_least_one_unused_token() throws Exception {
		this.tokens = this.tokenService.requestTokens(this.cpr, 1);
		this.tokenId = new TokenId(this.tokens.get(0).getTokenId());
	}

	@When("^The customer retrieves the token$")
	public void the_customer_retrieves_the_token() throws Exception {
		this.token = this.tokenService.getToken(this.cpr, this.tokenId);
	}
	
	@Then("^The customer gets back the token")
	public void the_customer_gets_back_the_token() throws Exception {
		assertEquals(this.tokens.get(0).getTokenId(), this.token.getTokenId());
	}
	
	@When("^The cpr number is requested from a token id belonging to that customer$")
	public void the_cpr_number_is_requested_from_a_token_id_belonging_to_that_customer() throws Exception {
	    this.cpr = this.tokenService.getCprNumberByTokenId(this.tokenId);
	}

	@Then("^The cpr number \"([^\"]*)\" is returned$")
	public void the_cpr_number_is_returned(String cpr) throws Exception {
		assertEquals(cpr, this.cpr.toString());
	}
	
}
