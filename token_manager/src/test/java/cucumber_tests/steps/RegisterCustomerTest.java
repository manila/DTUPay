package cucumber_tests.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;

public class RegisterCustomerTest {

	private TokenService tokenService;
	private TokenRepository tokenRepository;
	
	private String cprString;
	private CprNumber cpr;
	private String caughtErrMsg;
	
	public RegisterCustomerTest() {
		this.tokenRepository = new TokenRepositoryImpl();
		this.tokenService = new TokenServiceImpl(tokenRepository);
	}
	
	@Given("^An unregistered customer with a valid CPR number$")
	public void an_unregistered_customer_with_valid_cpr_number() throws Exception {
    	this.cprString = "1702921402";
	}

	@Given("^An unregistered customer with an invalid CPR number$")
	public void an_unregistered_customer_with_an_invalid_CPR_number() throws Exception {
	    this.cprString = "123@321";
	}
	
	@Given("^A registered customer with a valid CPR number$")
	public void a_registered_customer_with_a_valid_CPR_number() throws Exception {
	    this.cprString = "1702921402";
	    // simulate already registered
	    the_customer_registers_with_the_token_service();
	}

	@When("^The customer registers with the token service$")
	public void the_customer_registers_with_the_token_service() throws Exception {
    	try {
    		this.cpr = new CprNumber(cprString);
    		this.tokenService.addCustomer(cpr);
    	} catch (CprException e) {
    		this.caughtErrMsg = e.getMessage();
    	}
	}

	@Then("^The customer exists in the repository$")
	public void the_customer_exists_in_the_repository() throws Exception {
	    assertTrue(tokenService.customerExists(cpr));
	}

	@Then("^The customer has (\\d+) tokens$")
	public void the_customer_has_tokens(int expectedumTokens) throws Exception {
	    assertEquals(expectedumTokens, tokenService.getAllTokens(cpr).size());
	}	

	
	@Then("^Then an error message is returned saying \"([^\"]*)\"$")
	public void then_an_error_message_is_returned_saying(String expectedErrMsg) throws Exception {
		assertEquals(expectedErrMsg, this.caughtErrMsg);
	}
	
	@Then("^The customer does not exists in the repository$")
	public void the_customer_does_not_exists_in_the_repository() throws Exception {
	    assertTrue(!tokenService.customerExists(cpr));
	}
	
	
}
