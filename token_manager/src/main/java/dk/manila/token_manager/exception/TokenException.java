package dk.manila.token_manager.exception;

public class TokenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6623763784880245664L;

	public TokenException() {
	}

	public TokenException(String message) {
		super(message);
	}

}
