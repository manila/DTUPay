package dk.manila.token_manager.exception;

public class CprException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7748609687044491887L;

	public CprException(String msg) {
		super(msg);
	}
}
