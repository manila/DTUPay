package dk.manila.token_manager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 
 * @author sebastiannyholm
 * Abstract class to connect to Rabbitmq
 *
 */
public abstract class Connector {
    
	protected Channel channel;
    protected Connection connection;
    
    /**
     * 
     * @param queueName Name of the queue to connect
     * @param host Host of the queue
     * @throws IOException
     * @throws TimeoutException
     */
    public Connector(String queueName, String host) throws IOException, TimeoutException {
        
        ConnectionFactory connectionFactory = new ConnectionFactory();
        
        connectionFactory.setHost(host);
        connection = connectionFactory.newConnection();
        
        channel= connection.createChannel();
        channel.exchangeDeclare(queueName, "direct");
        
        channel.queueDeclare(queueName, false, false, false, null);
    }
 
    public void close() throws IOException, TimeoutException {
        this.channel.close();
        this.connection.close();
    }
}