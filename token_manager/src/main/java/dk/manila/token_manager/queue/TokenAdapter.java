package dk.manila.token_manager.queue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.TokenId;

/**
 * 
 * @author sebastiannyholm
 * Adapter to the TokenService for Rabbitmq
 *
 */
public class TokenAdapter extends Connector implements Consumer {
	
	private final static String QUEUE_NAME = "queue";
	private final static String EXCHANGE_HOST = "localhost";
	
	private TokenService tokenService;
	
	/**
	 * 
	 * @param tokenRepository The repository for the service
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public TokenAdapter(TokenRepository tokenRepository) throws IOException, TimeoutException {
		super(QUEUE_NAME, EXCHANGE_HOST);
		
		this.tokenService = new TokenServiceImpl(tokenRepository);
	}
	
	/**
	 * Start listen to the queue
	 * 
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public void runQueue() throws IOException, TimeoutException {
		
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		
		String routingKey = "fromCustomerToToken";
		
		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, QUEUE_NAME, routingKey);
		channel.basicConsume(queueName, true, this);
	}

	@Override
	public void handleCancel(String info) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Cancel"+ info);
	}

	@Override
	public void handleCancelOk(String info) {
		// TODO Auto-generated method stub
		System.out.println("CancelOk"+ info);
	}

	@Override
	public void handleConsumeOk(String info) {
		// TODO Auto-generated method stub
		System.out.println("ConsumeOk"+ info);
	}

	@Override
	public void handleDelivery(String info, Envelope env, BasicProperties props, byte[] body) throws IOException {
		
		System.out.println(" [*] Recieved new message");
		
		ArrayList<Object> response = SerializationUtils.deserialize(body);
		
		String corrId = (String) response.get(0);
		String operation = (String) response.get(1);
		
		boolean doCallback = false;
		String callbackRouteKey = "";
		String callbackOperation = operation + "Callback";
		
		String message = "No message";
		
		System.out.println(" [*] " + corrId + " | Message ID");
		
		switch (operation) {
			case "addCustomerToTokenRepository":
				
				doCallback = true;
				callbackRouteKey = "fromTokenToCustomer";
				
				String cprString = (String) response.get(2);
				
				try {
					CprNumber cpr = new CprNumber(cprString);	
					this.tokenService.addCustomer(cpr);
					message = cpr + " added to repository";
						
					
				} catch (CprException e) {
					message = e.getMessage();
				}
				
				break;
				
			case "getCprNumberByTokenId":
				
				doCallback = true;
				callbackRouteKey = "fromTokenToCustomer";
				
				String tokenIdString = (String) response.get(2);
				
				TokenId tokenId = new TokenId(tokenIdString);
				
				try {
					CprNumber cprNumber = this.tokenService.getCprNumberByTokenId(tokenId);
					
					message = cprNumber + " found from the token id: " + tokenId;
					
				} catch (TokenException e) {
					message = e.getMessage();
				}
				break;
				
			default:
				doCallback = true;
				message = "Operation not found";
				break;
		}
	    
		if (doCallback) {
			System.out.println(" [*] " + corrId + " | Sending callback");
			
		    ArrayList<Object> object = new ArrayList<Object>();
		    object.add(corrId);
		    object.add(callbackOperation);
		    object.add(message);
		    
		    channel.basicPublish(QUEUE_NAME, callbackRouteKey, null, SerializationUtils.serialize(object));
		}
		
		System.out.println(" [*] " + corrId + " | End of message");
	}
	
	@Override
	public void handleShutdownSignal(String info, ShutdownSignalException exception) {
		// TODO Auto-generated method stub
		System.out.println("ShutDownSignal" + info);
	}

	@Override
	public void handleRecoverOk(String info) {
		// TODO Auto-generated method stub
		System.out.println("HandleRecoverOk " + info);
	}

}

