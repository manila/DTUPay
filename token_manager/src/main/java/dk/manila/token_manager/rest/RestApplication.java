package dk.manila.token_manager.rest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.queue.TokenAdapter;

@ApplicationPath("/")
public class RestApplication extends Application {
	
	public RestApplication() {
		
		try {
			TokenAdapter tokenAdapter = new TokenAdapter(TokenRepositoryImpl.getInstance());
			tokenAdapter.runQueue();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
	
}
