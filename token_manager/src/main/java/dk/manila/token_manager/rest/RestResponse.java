package dk.manila.token_manager.rest;

import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

public class RestResponse {

	private int statusCode; 
	private String message;
	private Object data;
	
	private JSONObject json;
	
	public RestResponse(int statusCode, String message, Object object) {
		this.statusCode = statusCode;
		this.message = message;
		this.data = object;
		
		this.json = new JSONObject();
	}
	
	public Response getResponse() {
		
		try {
			this.json.put("statusCode", this.statusCode);
			this.json.put("message", this.message);
			this.json.put("tokens", this.data);
			
		} catch (JSONException e) {
			
			this.statusCode = 400;
			this.message = e.getMessage();
			
			try {
				this.json.put("statusCode", this.statusCode);
				this.json.put("message", this.message);
				
			} catch (JSONException e1) {
				
				return Response.status(this.statusCode).build();
			}
		}
		
		return Response.status(this.statusCode).entity(this.json.toString()).build();
	}
}
