package dk.manila.token_manager.rest.input_wrappers;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RequestTokenModel {

	@XmlElement public int quantity;
	
}
