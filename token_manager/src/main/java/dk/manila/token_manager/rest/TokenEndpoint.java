package dk.manila.token_manager.rest;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.zxing.WriterException;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.impl.TokenRepositoryImpl;
import dk.manila.token_manager.impl.TokenServiceImpl;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.model.TokenRepresentation;
import dk.manila.token_manager.rest.input_wrappers.RequestTokenModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

/**
 * 
 * @author sebastiannyholm
 * Endpoint for /tokens
 * Only registered customers should have access
 * If not registered, no access is returned
 *
 */
@Path("/tokens")
public class TokenEndpoint {
	  
	private TokenService tokenService;
	
	public TokenEndpoint() {
		
		this.tokenService = new TokenServiceImpl(TokenRepositoryImpl.getInstance());
	}
	
	@GET
	@Path("/{id}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getToken(@Context HttpHeaders header, @PathParam("id") TokenId id) {
		
		CprNumber cpr;
		try {
			cpr = authorize(header);
		} catch (Exception e) {
			return new RestResponse(401, e.getMessage(), null).getResponse();
		}
		
		TokenRepresentation token;
		try {
			token = this.tokenService.getToken(cpr, id);
		} catch (TokenException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		} catch (CprException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		}
		
		List<TokenRepresentation> tokens = new ArrayList<TokenRepresentation>();
		tokens.add(token);
		
		return new RestResponse(200, "Success", tokens).getResponse(); 
	}
	
	@GET
	@Path("/{id}/image")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces("image/png")
	public Response getBarcode(@Context HttpHeaders header, @PathParam("id") TokenId id) {
		
		CprNumber cpr;
		try {
			cpr = authorize(header);
		} catch (Exception e) {
			return new RestResponse(401, e.getMessage(), null).getResponse();
		}
		
		TokenRepresentation token;
		try {
			token = this.tokenService.getToken(cpr, id);
		} catch (TokenException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		} catch (CprException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		}
				
		byte[] bytes;
		try {
			bytes = token.generateBarcodeImage();
		} catch (WriterException e) {
			return new RestResponse(500, e.getMessage(), null).getResponse();
		} catch (IOException e) {
			return new RestResponse(500, e.getMessage(), null).getResponse();
		}
		
		return Response.ok(bytes).build(); // new RestResponse(200, "Success", token).getResponse(); 
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response requestTokens(@Context HttpHeaders header, RequestTokenModel requestTokenModel) {
		
		CprNumber cpr;
		try {
			cpr = authorize(header);
		} catch (Exception e) {
			return new RestResponse(401, e.getMessage(), null).getResponse();
		}
		
		List<TokenRepresentation> tokens;
		
		try {
			tokens = this.tokenService.requestTokens(cpr, requestTokenModel.quantity);
		} catch (TokenException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		} catch (CprException e) {
			return new RestResponse(400, e.getMessage(), null).getResponse();
		}
		
		return new RestResponse(200, "Success", tokens).getResponse(); 
	}
	
	private CprNumber authorize(HttpHeaders header) throws Exception  {
		
		String authorization = header.getHeaderString("Authorization");
		String cprString = "";
		CprNumber cpr;
		
		try {
			cprString = authorization.split(" ")[1];	
		} catch (Exception e) {
			throw new Exception("Invalid authorization format");
		}
		
		try {
			cpr = new CprNumber(cprString);
		} catch (CprException e) {
			throw new CprException("Invalid cpr format");
		}
		
		return cpr;
	}
}
