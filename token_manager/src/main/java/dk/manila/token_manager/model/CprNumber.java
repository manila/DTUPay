package dk.manila.token_manager.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.manila.token_manager.exception.CprException;

public class CprNumber extends IdValueObject {
	String cprRegex = "\\d{10}";
	
	public CprNumber(String cpr) throws CprException {
		super(cpr);
        Pattern pattern = Pattern.compile(cprRegex);
        Matcher matcher = pattern.matcher(cpr);
        if (!matcher.matches()) throw new CprException("Invalid CPR number");
	}
	
}