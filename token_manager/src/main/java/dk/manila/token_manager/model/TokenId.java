package dk.manila.token_manager.model;

public class TokenId extends IdValueObject {
	
	public TokenId(String tokenId) {
        super(tokenId);
	}
}