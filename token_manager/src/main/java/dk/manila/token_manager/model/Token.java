package dk.manila.token_manager.model;

import java.util.UUID;

public class Token {

	private TokenId tokenId;
	private boolean used;
	
	public Token() {
		this.tokenId = new TokenId(UUID.randomUUID().toString());
		this.used = false;
	}
	
	public TokenId getTokenId() {
		return tokenId;
	}
	
	public boolean isUsed() {
		return used;
	}
	
	public void setUsed(boolean used) {
		this.used = used;
	}
}
