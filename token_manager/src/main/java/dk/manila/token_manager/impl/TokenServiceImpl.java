package dk.manila.token_manager.impl;

import java.util.ArrayList;
import java.util.List;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.interfaces.TokenService;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.model.TokenRepresentation;

public class TokenServiceImpl implements TokenService {
	
	private TokenRepository tokenRepository;
	
	public TokenServiceImpl(TokenRepository tokenRepository) {
		this.tokenRepository = tokenRepository;
	}
	
	@Override
	public List<TokenRepresentation> requestTokens(CprNumber cpr, int quantity) throws TokenException, CprException {
		if (!tokenRepository.containsCustomer(cpr)) throw new CprException("CPR number not registered as a customer");
		if (quantity < 1) throw new TokenException("Cannot request less than one token");
		if (quantity > 5) throw new TokenException("Cannot request more than five tokens");
		if (this.getUnusedTokens(cpr).size() > 1) throw new TokenException("Too many unused tokens to request any more");
		
		List<Token> tokens = generateTokens(quantity);
		tokenRepository.addTokens(cpr, tokens);
		return generateTokenRepresentations(tokens);
	}

	private List<Token> generateTokens(int n) {
		List<Token> tokens = new ArrayList<Token>();
		for (int i = 0; i < n; i++) {
			tokens.add(new Token());
		}
		return tokens;
	}
	
	private List<TokenRepresentation> generateTokenRepresentations(List<Token> tokens) {
		List<TokenRepresentation> tokenReps = new ArrayList<TokenRepresentation>();
		for (Token t : tokens) {
			tokenReps.add(new TokenRepresentation(t));
		}
		return tokenReps;
	}

	@Override
	public void addCustomer(CprNumber cpr) throws CprException {
		if (tokenRepository.containsCustomer(cpr))
			throw new CprException("A customer with this cpr is already registered");
		this.tokenRepository.addCustomer(cpr);
	}

	@Override
	public TokenRepresentation getToken(CprNumber cpr, TokenId tokenId) throws TokenException, CprException {	
		Token t = this.tokenRepository.getToken(cpr, tokenId);
		return new TokenRepresentation(t);
	}

	@Override
	public CprNumber getCprNumberByTokenId(TokenId tokenId) throws TokenException {
		
		CprNumber cpr = tokenRepository.getCprByTokenId(tokenId);

		
		if (cpr == null)
			throw new TokenException("No token with specified id");
			
		return cpr; 
		
	}

	@Override
	public List<TokenRepresentation> getUnusedTokens(CprNumber cpr) {
		List<TokenRepresentation> unusedTokens = new ArrayList<TokenRepresentation>();
		
		for (Token t : tokenRepository.getTokens(cpr)) {
			if (!t.isUsed()) unusedTokens.add(new TokenRepresentation(t));
		}
		return unusedTokens;
	}

	@Override
	public boolean customerExists(CprNumber cpr) {
		return tokenRepository.containsCustomer(cpr);
	}

	@Override
	public List<TokenRepresentation> getAllTokens(CprNumber cpr) {
		List<TokenRepresentation> tokens = new ArrayList<TokenRepresentation>();
		
		for (Token t : tokenRepository.getTokens(cpr)) {
			tokens.add(new TokenRepresentation(t));
		}
		
		return tokens;
	}
}













