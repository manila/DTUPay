package dk.manila.token_manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.interfaces.TokenRepository;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;

	public class TokenRepositoryImpl implements TokenRepository {
	
	private static final TokenRepository TOKEN_REPOSITORY_INSTANCE = new TokenRepositoryImpl();
	
	public static TokenRepository getInstance() {
		return TOKEN_REPOSITORY_INSTANCE;
	}
	
	private Map<CprNumber, List<Token>> tokens;
	
	public TokenRepositoryImpl() {
		tokens = new HashMap<CprNumber, List<Token>>();
	}
	
	
	public void addToken(CprNumber cpr, Token token) {
		tokens.get(cpr).add(token);
	}
	
	
	public void addTokens(CprNumber cpr, List<Token> tokens) {
		for (Token t : tokens)
			addToken(cpr, t);
	}
	

	public boolean containsCustomer(CprNumber cpr) {
		return tokens.containsKey(cpr);
	}

	public void addCustomer(CprNumber cpr) {
		tokens.put(cpr, new ArrayList<Token>());
	}
	
	public Token getToken(CprNumber cpr, TokenId tokenId) {
		
		for (Token t : tokens.get(cpr))
			if (t.getTokenId().equals(tokenId))
				return t;
		
		return null;
	}

	public CprNumber getCprByTokenId(TokenId tokenId) {
		
		for (CprNumber cpr : tokens.keySet())
			for (Token t : tokens.get(cpr))
				if (t.getTokenId().equals(tokenId))
					return cpr;
		return null;
	}


	@Override
	public List<Token> getTokens(CprNumber cpr) {
		List<Token> myTokens = new ArrayList<Token>();
		
		if (tokens.containsKey(cpr)) {
			myTokens = tokens.get(cpr);
		}
		
		return myTokens;
	}

}