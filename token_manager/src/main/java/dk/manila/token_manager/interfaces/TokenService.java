package dk.manila.token_manager.interfaces;

import java.util.List;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.TokenId;
import dk.manila.token_manager.model.TokenRepresentation;

/**
 * 
 * @author lukasvillumsen
 * Service containing all methods needed for the service
 */
public interface TokenService {
	
	/**
	 * Request tokens
	 * 
	 * @param cpr The customers CprNumber
	 * @param quantity The amount of Tokens to generate (between 1-5)
	 * @return tokenRepresentations A list of tokens that has been generated
	 */
	public List<TokenRepresentation> requestTokens(CprNumber cpr, int quantity) throws TokenException, CprException;
	
	/**
	 * Add customer to the repository
	 * 
	 * @param cpr The CprNumber of a new customer
	 * @throws CprException 
	 */
	public void addCustomer(CprNumber cpr) throws CprException;

	/**
	 * 
	 * @param cprNumber The CprNumber of a customer
	 * @param tokenId The Tokens id that the customer wants to fetch
	 * @return A TokenRepresentation of the Token
	 * @throws TokenException
	 * @throws CprException
	 */
	public TokenRepresentation getToken(CprNumber cprNumber, TokenId tokenId) throws TokenException, CprException;

	/**
	 * Get the CprNumber of a customer that has the given token
	 * 
	 * @param tokenId The ID of a Token belonging to the customer
	 * @return The CprNumber of the customer that has the given token
	 * @throws TokenException
	 */
	public CprNumber getCprNumberByTokenId(TokenId tokenId) throws TokenException;

	/**
	 * Fetches all tokens marked as unused belonging to a CPR number 
	 * @param cpr The CPR number to fetch tokens from
	 * @return unusedTokens List of token representations of unused tokens
	 * @throws CprException
	 */
	public List<TokenRepresentation> getUnusedTokens(CprNumber cpr);

	
	/**
	 * Checks if a customer with the specified CPR number exists in the repository
	 * @param cpr The CPR number to check for
	 * @return exists Boolean value reflecting the existence of the customer in the repository
	 */
	public boolean customerExists(CprNumber cpr);
	
	
	
	/**
	 * Retrieves all tokens belonging to a customer with the specified CPR number
	 * @param cpr The CPR number to fetch for
	 * @return tokens List of token representation for all the tokens belonging to the CPR
	 */
	public List<TokenRepresentation> getAllTokens(CprNumber cpr);
}


















