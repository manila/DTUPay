package dk.manila.token_manager.interfaces;

import java.util.List;

import dk.manila.token_manager.exception.CprException;
import dk.manila.token_manager.exception.TokenException;
import dk.manila.token_manager.model.CprNumber;
import dk.manila.token_manager.model.Token;
import dk.manila.token_manager.model.TokenId;

/**
 * 
 * @author sebastiannyholm
 * Repository containing all methods needed for the TokenManager service  
 *
 */
public interface TokenRepository {
	
	/**
	 * Adds a token to the token repository for a given CprNumber
	 * Simply updates the repository without returning anything
	 * 
	 * @param cpr The CprNumber to associate with the token @see dk.manila.token_manager.model.CprNumber
	 * @param token The token to add to the repository @see dk.manila.token_manager.model.Token
	 */
	public void addToken(CprNumber cpr, Token t);
	
	/**
	 * Updates the repository, individually adding tokens from a list to a specified CprNumber
	 * 
	 * @param cpr The CprNumber to associate with the tokens @see dk.manila.token_manager.model.CprNumber
	 * @param tokens A list of tokens to add to a CprNumber
	 */
	public void addTokens(CprNumber cpr, List<Token> tokens);

	
	/**
	 * Checks if a CprNumber exists in the repository
	 * 
	 * @param cpr The CprNumber
	 * @return exists Boolean value reflecting the presence of the cpr in the repository
	 */
	public boolean containsCustomer(CprNumber cpr);
	
	/**
	 * Add customer to the map
	 *
	 * @param cpr The CprNumber
	 */
	public void addCustomer(CprNumber cpr);
	
	/**
	 * Return a token
	 *
	 * @param cpr The CprNumber
	 * @param tokenId The TokenId of a token
	 * @return token the token with the given TokenId
	 */
	public Token getToken(CprNumber cpr, TokenId tokenId);

	/**
	 * Return the CprNumber that owns the given TokenId
	 *
	 * @param tokenId The TokenId of a token
	 * @return cpr The CprNumber that owns the given TokenId
	 */
	public CprNumber getCprByTokenId(TokenId tokenId);

	
	/**
	 * Retrieves the list of tokens associated with a given cpr
	 * @param cpr The CprNumber 
	 * @return 
	 */
	public List<Token> getTokens(CprNumber cpr);
	
}


























