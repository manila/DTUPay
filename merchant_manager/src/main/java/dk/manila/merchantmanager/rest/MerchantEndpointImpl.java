package dk.manila.merchantmanager.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.merchantmanager.merchantservice.MerchantService;
import dk.manila.merchantmanager.merchantservice.MerchantServiceImpl;
import dk.manila.merchantmanager.queue.MerchantQueue;
import dk.manila.merchantmanager.queue.MerchantQueueImpl;
import dk.manila.merchantmanager.repository.MerchantRepositoryImpl;


@Path("/merchants")
public class MerchantEndpointImpl {
	private MerchantQueue queue;
	private MerchantService service;
	public MerchantEndpointImpl() throws IOException, TimeoutException {
		queue = new MerchantQueueImpl();
		service = new MerchantServiceImpl(MerchantRepositoryImpl.INSTANCE_MERCHANT_REPOSITORY, queue);
	}

	@POST
	@Path("")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response createMerchant(String merchant) {
		if(service.contains(merchant))
			return Response.status(400).entity(merchant + "is already registered").build();
		ArrayList<Object> arg = new ArrayList<>();
		arg.add(merchant);
		try {
			queue.execute("get_account_id_by_cpr", arg);
		} catch (IOException | TimeoutException e) {
			return Response.status(500).build();
		}

		return Response.ok().entity(merchant).build();

	}

}
