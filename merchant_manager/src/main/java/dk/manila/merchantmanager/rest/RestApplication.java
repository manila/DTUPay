package dk.manila.merchantmanager.rest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.merchantmanager.queue.MerchantQueue;
import dk.manila.merchantmanager.queue.MerchantQueueImpl;


@ApplicationPath("/")
public class RestApplication extends Application {
	
	public RestApplication() throws IOException, TimeoutException {
		MerchantQueue queue = new MerchantQueueImpl();
		queue.execute("listener", null);
		

	}

}
