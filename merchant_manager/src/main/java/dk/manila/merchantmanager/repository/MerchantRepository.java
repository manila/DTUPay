package dk.manila.merchantmanager.repository;

import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;

public interface MerchantRepository {
		
	void addMerchant(Merchant merchant);
	
	String getMerchantAccountId(CprNumber cpr);
	
	boolean contains(CprNumber cprNumber);

}
