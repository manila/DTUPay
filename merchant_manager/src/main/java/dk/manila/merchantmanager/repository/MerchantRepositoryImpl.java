package dk.manila.merchantmanager.repository;

import java.util.HashSet;
import java.util.Set;

import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;


public class MerchantRepositoryImpl implements MerchantRepository {
	
	public static final MerchantRepositoryImpl INSTANCE_MERCHANT_REPOSITORY = new MerchantRepositoryImpl();
	private MerchantRepositoryImpl() {

	}
	
	private final Set<Merchant> merchants = new HashSet<>();
	
	public Merchant getMerchantByCpr(CprNumber cpr) {

		for (Merchant m : merchants) {
			if (m.getCprNumber().equals(cpr)) {

				return m;
			}

		}

		return null;
	}
	
	@Override
	public void addMerchant(Merchant merchant) {
		if(merchant==null)
			throw new NullPointerException();
		merchants.add(merchant);
	}
	
	@Override
	public String getMerchantAccountId(CprNumber cpr) {
		for(Merchant m:merchants) {
			if(m.getCprNumber().equals(cpr))
				return m.getBankAccount_id();
		}
		return null;
	}
	
	@Override
	public boolean contains(CprNumber cprNumber) {
		for(Merchant m:merchants)
			if(m.getCprNumber().equals(cprNumber))
				return true;
		return false;
	}

}
