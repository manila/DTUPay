package dk.manila.merchantmanager.merchantservice;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface MerchantService {
	
	void registerMerchant(String cpr, String bankAccountId) throws IOException, TimeoutException;

	String getMerchantAccountId(String cpr);

	boolean contains(String cpr);

}
