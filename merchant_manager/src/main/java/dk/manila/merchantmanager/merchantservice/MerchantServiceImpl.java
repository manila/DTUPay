package dk.manila.merchantmanager.merchantservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import dk.manila.merchantmanager.queue.MerchantQueue;
import dk.manila.merchantmanager.repository.MerchantRepository;
import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;



public class MerchantServiceImpl implements MerchantService {
	
	private final MerchantRepository merchantRepository;
	private final MerchantQueue queue;
	
	public MerchantServiceImpl(MerchantRepository merchantRepository, MerchantQueue queue) throws IOException, TimeoutException {
		this.merchantRepository = merchantRepository;
		this.queue = queue;
	}

	@Override
	public String getMerchantAccountId(String cpr) {
		return this.merchantRepository.getMerchantAccountId(new CprNumber(cpr));
	}
	
	@Override
	public void registerMerchant(String cpr, String bankAccountId) throws IOException, TimeoutException {
		
		CprNumber cprNumber = new CprNumber(cpr);
		Merchant merchant = new Merchant(cprNumber, bankAccountId);
		merchantRepository.addMerchant(merchant);
		ArrayList<Object> arg = new ArrayList<>();
		arg.add(cpr);
		queue.execute("new_merchant_notification",arg);	
	}
	
	@Override
	public boolean contains(String cpr) {
		CprNumber cprNumber = new CprNumber(cpr);
		return merchantRepository.contains(cprNumber);
	}


}
