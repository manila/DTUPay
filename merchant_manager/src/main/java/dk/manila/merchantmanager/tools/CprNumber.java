package dk.manila.merchantmanager.tools;


public class CprNumber {
	
	private final String cpr;
	public CprNumber(String cpr) {
		this.cpr = cpr;
	}
	public String getCpr() {
		return cpr;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpr == null) ? 0 : cpr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CprNumber other = (CprNumber) obj;
		if (cpr == null) {
			if (other.cpr != null)
				return false;
		} else if (!cpr.equals(other.cpr))
			return false;
		return true;
	}
	


}
