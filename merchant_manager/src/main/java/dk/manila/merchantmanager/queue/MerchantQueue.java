package dk.manila.merchantmanager.queue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

public interface MerchantQueue {
	
	public void execute(String instruction, ArrayList<Object> arg) throws IOException, TimeoutException;
}
