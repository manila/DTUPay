package dk.manila.merchantmanager.merchantservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;

import dk.manila.merchantmanager.queue.MerchantQueue;
import dk.manila.merchantmanager.repository.MerchantRepository;
import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;


public class MerchantServiceTest {
	private MerchantRepository repository;
	private MerchantQueue queue;
	private MerchantService service;
	private String cpr;
	private String bankAccountId;
	@Before
	public void initialisation() throws IOException, TimeoutException {
		repository = new MerchantRepository() {
			private Merchant merchant;
			@Override
			public String getMerchantAccountId(CprNumber cpr) {
				return merchant.getBankAccount_id();
			}
			
			@Override
			public void addMerchant(Merchant merchant) {
				this.merchant = merchant;
			}

			@Override
			public boolean contains(CprNumber cprNumber) {
				if(merchant!=null)
					return merchant.getCprNumber().equals(cprNumber);
				return false;
			}
		};
		queue = new MerchantQueue() {
			
			@Override
			public void execute(String instruction, ArrayList<Object> arg) throws IOException, TimeoutException {
				// TODO Auto-generated method stub
				
			}
		};
		service = new MerchantServiceImpl(repository, queue);
		cpr = "654321-1234";
		bankAccountId = "6789";
		
	}
	@Test
	public void add_new_merchant_to_repository() throws IOException, TimeoutException {
		service.registerMerchant(cpr, bankAccountId);
		assertEquals(bankAccountId, service.getMerchantAccountId(cpr));
	}
	
	@Test
	public void contains_cpr_with_a_registerMerchant() throws IOException, TimeoutException {
		service.registerMerchant(cpr, bankAccountId);
		assertTrue(service.contains(cpr));
	}
	@Test
	public void contains_with_a_non_registered_cpr() {
		assertFalse(service.contains(cpr));
	}
	
	
}
