package dk.manila.merchantmanager.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import dk.manila.merchantmanager.tools.CprNumber;
import dk.manila.merchantmanager.tools.Merchant;

public class MerchantRepositoryTest {
	private MerchantRepository repository;
	private Merchant merchant;

	@Before
	public void initialisation() {
		merchant = new Merchant(new CprNumber("654321-1234"), "6789");
		repository = MerchantRepositoryImpl.INSTANCE_MERCHANT_REPOSITORY;
	}

	@Test
	public void add_a_new_merchant() {
		repository.addMerchant(merchant);
		assertTrue(repository.contains(merchant.getCprNumber()));
	}

	@Test(expected = NullPointerException.class)
	public void add_a_null_merchant_and_receive_null_exception() {
		repository.addMerchant(null);
	}
	
	@Test
	public void add_a_merchant_and_check_if_the_set_contains_it() {
		repository.addMerchant(merchant);
		assertTrue(repository.contains(merchant.getCprNumber()));
	}
	
	@Test
	public void contains_with_a_nonregistered_merchant() {
		assertFalse(repository.contains(merchant.getCprNumber()));
	}
	@Test
	public void getMerchantAccountId_with_a_register_merchant() {
		repository.addMerchant(merchant);
		assertEquals(merchant.getBankAccount_id(), repository.getMerchantAccountId(merchant.getCprNumber()));
	}
}
